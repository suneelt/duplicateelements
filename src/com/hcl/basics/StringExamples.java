//string is a sequence of characters written in double quotes
package com.hcl.basics;

import java.util.Arrays;
import java.util.Scanner;

public class StringExamples {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//1.toCharArray:it converts the String into Character array
		
		String text1="Suneelkumar";
		
		char ch[]=text1.toCharArray();
		
		for(char foreach:ch) {
			System.out.println(foreach);
			
		}
		Scanner scanner=new Scanner(System.in);
		System.out.println("Enter the name");
		String name=scanner.nextLine();
		char ch1[]=name.toCharArray();
		for(int i=0;i<ch1.length;i++) {
		 
		  System.out.println(ch1[i]);
		  
		}
		Arrays.sort(ch1);
		for(int i=0;i<ch1.length;i++) {
			 
			  System.out.println(ch1[i]);
			  
			}
		
		

	}

}
