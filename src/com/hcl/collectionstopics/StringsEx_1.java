package com.hcl.collectionstopics;

public class StringsEx_1 {


	public void stringCompareToEQualsConcatMethod() {
		String text1="suneelkumar";
		String text2="ramu";
		System.out.println(text1.equals(text2));
		System.out.println(text1.concat(text2));
		System.out.println(text1.compareTo(text2));
	}
	public void toCharArrayEx() {
		String text="TestLeaf";
		char[] charArray = text.toCharArray();

		for(char letters:charArray) {
			System.out.print(letters);
		}

		//length
		int count = text.length();
		System.out.println(count);
		//charAt for finding the letter

		char charAt = text.charAt(5);
		System.out.println(charAt);

		//indexof :index position of the letter

		int indexposition=text.indexOf('a');
		System.out.println(indexposition);

		//concat :we are going to adding the two words
		text=text.concat("chinna");
		System.out.println(text);

		//sustring:beginindex and end index
		String text1="rajTramurani";
		text1=text1.substring(0,5);
		System.out.println(text1);

		//trim:remove the spaces eiether starting or ending
		String text2="  suneelkumar  ";
		text2=text2.trim();
		System.out.println(text2);

		//lowercase and upper case
		String lowerCase = text1.toLowerCase();
		System.out.println(lowerCase);
		text2=text2.toUpperCase();
		System.out.println(text2);

		//contains and startswith and endswith

		String str="TestLeaf";
		System.out.println(str.startsWith("s"));
		System.out.println(str.endsWith("r"));
		System.out.println(str.contains("z"));

		//replace

		str=str.replace("f", "s");
		System.out.println(str);

		//ToString any type of data(int ,char) we are conerting string

		String num="1323";
		String string = num.toString();

		System.out.println(string);

		//String Buffer

		/*StringBuffer e=new StringBuffer("suneel");
		e.reverse();*/






	}
	public static void main(String args[]) {
		StringsEx_1 ex=new StringsEx_1();
		ex.stringCompareToEQualsConcatMethod();
		ex.toCharArrayEx();

		StringBuffer e=new StringBuffer("suneel");
		System.out.println(e.reverse());

	}

}
